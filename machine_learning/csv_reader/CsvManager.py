import csv

from machine_learning.util.utils import map_time_to_circle


def read_csv_file(csv_file,delimiter=';'):
    with open(csv_file, 'rt') as csvfile:
        csvreader = csv.reader(csvfile, delimiter=delimiter)
        dataset = list()
        for row in csvreader:
            if not row:
                continue

            if(row[0].isdigit()):
                # If the row is in hour;minute format we map hour and minute to the circle. This is done in order
                # to be able to calculate correct distances between hours
                circle_cordinates = map_time_to_circle(int(row[0]), int(row[1]))
                row[0] = circle_cordinates[0]
                row[1] = circle_cordinates[1]
            dataset.append(row)
        return dataset

def write_csv_file(csvdata, csv_file,delimiter=';'):
    with open(csv_file, 'w+') as csvfile:
        csvwriter = csv.writer(csvfile, delimiter=delimiter)
        for row in csvdata:
            csvwriter.writerow(row)