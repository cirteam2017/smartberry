from networking.NetworkManager import NetworkManager

CLASSIFIER_URL = "http://localhost:10010"


class CommandClassifier:

    def __init__(self):
        self.networkManager = NetworkManager()

    def classify(self,sentence):
        return self.networkManager.jsonPutRequest("%s/classify" % CLASSIFIER_URL, {"sentence": sentence})