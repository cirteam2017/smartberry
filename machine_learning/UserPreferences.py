"""
    Algorithm: random forest

    Features to be considered for the machine learning algorithm for the lightning:

    time
    switch_state (Class)

    the class will be the switch state
        SWITCH_ON, SWITCH_OFF
"""
import sys
import traceback

from sklearn.neighbors import KNeighborsClassifier

from machine_learning.csv_reader.CsvManager import read_csv_file
from machine_learning.csv_reader.CsvManager import write_csv_file
from machine_learning.util.utils import map_time_to_circle

DATASET_ML_CSV = 'dataset/DatasetML.csv'


class UserPreferences:

    def __init__(self):
        self.neigh = KNeighborsClassifier(n_neighbors=2)
        self.data = self.read_dataset_csv()

    def add_instance_to_dataset(self,instance):
        instance["time"] = map_time_to_circle(instance["hour"],instance["hour"])
        del instance["hour"]
        del instance["minute"]
        self.data.append([instance["time"][0],
                     instance["time"][1],
                     instance["switch_status"]])
        write_csv_file(self.data,DATASET_ML_CSV)
        self.train()


    def read_dataset_csv(self):
        data = None
        try:
            data = read_csv_file(DATASET_ML_CSV)
        except:
            traceback.print_exc(file=sys.stdout)
        if data is None:
            data = []
        return data

    def train(self):
        X = [item[0:-1] for item in self.data]
        y = [item[-1] for item in self.data]
        self.neigh.fit(X, y)

    def predict(self,instance):
        circle = map_time_to_circle(instance["hour"], instance["minute"])
        return self.neigh.predict([[circle[0], circle[1]]])

