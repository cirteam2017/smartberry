import numpy as np


def map_time_to_circle(hour,minute):
    minute_10 = minute/60
    hour = hour+minute_10
    pi_value = (hour / 24) * (2 * np.pi)
    return [np.cos(pi_value),np.sin(pi_value)]