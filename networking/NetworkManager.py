import json

import requests

class NetworkManager:

    def jsonGetRequest(self,url):
        try:
            r = requests.get(url)
            return r.json()
        except:
            return None

    def jsonPutRequest(self,url,body):
        try:
            # bodyJson = json.dumps(body)
            headers = {'Content-Type': 'application/json'}
            r = requests.put(url,json=body,headers=headers)
            return r.json()
        except:
            return None