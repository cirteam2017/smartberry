import sys
import signal
import os.path
import threading
from threading import Thread

import datetime
import speech_recognition as sr
import logging
from emotion.Emotion import Emotion
from time import gmtime, strftime
import random

import time

from machine_learning.UserPreferences import UserPreferences
from rf_library.RFLibrary import RFLibrary
from rf_library.Message import Message

from conversation.Conversation import Conversation
from conversation.TextToSpeech import TextToSpeech
from machine_learning.command_analyzer.CommandClassifier import CommandClassifier
from visual.LedDriver import LedDriver

SHOULD_START_PERIODIC_CHECK = False

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger("SmartBerry")

SNOWBOY_PYTHON_PATH = "/etc/snowboy/swig/Python/"
SNOWBOY_PYTHON_PATH_2 = "/etc/snowboy/examples/Python/"
SNOWBOY_FILES = ["snowboydetect.py","_snowboydetect.so"]
SNOWBOY_FILES_2 = ["snowboydecoder.py"]
SNOWBOY_RESOURCE = "/etc/snowboy/"
SNOWBOY_RESOURCE_FILE = ["resources"]
interrupted = False

command_answers = ["At your service", "Sure", "I will do it", "No problem", "Ok", "It will be a pleasure"]
goodbyes = ["Bye", "Goodbye", "See you", "See you soon", "Bye bye", "So long", "Have a good one", "Take care"]

model = "model/Hey buddy!.pmdl"
detector = None

conversation = Conversation()
commandClassifier = CommandClassifier()
textToSpeech = TextToSpeech()
ledDriver = LedDriver()
jsonCredentialsFile = None
current_status = dict()
current_status["switch_1"] = 0
with open('SmartBerry-99c2bc584f3b.json', 'r') as content_file:
    jsonCredentialsFile = content_file.read()

def create_symbolink_links(file_names,src_dir):
    for file in file_names:
        if not os.path.exists(file):
            logger.debug('Created symlink to: ' + file)
            os.symlink(src_dir + file, file)

def initialize_snowboy():
    create_symbolink_links(SNOWBOY_FILES,SNOWBOY_PYTHON_PATH)
    create_symbolink_links(SNOWBOY_FILES_2,SNOWBOY_PYTHON_PATH_2)
    create_symbolink_links(SNOWBOY_RESOURCE_FILE,SNOWBOY_RESOURCE)

initialize_snowboy()

rf = RFLibrary()
rf.public_initialize(2,2,0,0)
usr = UserPreferences()

periodic_check_thread = None


import snowboydecoder

def signal_handler(signal, frame):
    global interrupted
    interrupted = True

def execute_action(action):
    if action == "TURN_ON_LIGHT":
        msg = Message(5, 2)
        msg.add_value(0x30, 0)
        current_status["switch_1"] = 1
        rf.send_message(msg)
    elif action == "TURN_OFF_LIGHT":
        msg = Message(5, 2)
        msg.add_value(0x30, 1)
        current_status["switch_1"] = 0
        rf.send_message(msg)
    elif action == "OPEN_BLINDERS":
        msg = Message(6, 2)
        msg.add_value(0x30, 1)
        rf.send_message(msg)
    elif action == "CLOSE_BLINDERS":
        msg = Message(6, 2)
        msg.add_value(0x30, 0)
        rf.send_message(msg)
    elif action == "READ_TIME":
        return "It's %s" % strftime("%H:%M", time.localtime())
    return random.choice(command_answers)


def read_text(reply):
    textToSpeech.read_text(reply)

def read_text_async(reply):
    textToSpeech.read_text(reply)

def hotword_detected():
    global detector,conversation
	# We should start the speech recognition here
    logger.debug("Hotword detected")
    ledDriver.set_emotion(Emotion.LISTENING)
    detector.terminate()
    text = listen_for_speech()
    if text:
        action = commandClassifier.classify(text)
        print(action)
        if len(action) == 0:
            if text is not None:
                ledDriver.set_emotion(Emotion.CONVERSATION_ANSWERING)
                reply = conversation.getReply(text)
                logger.debug("Conversation Answer: " + reply)
        else:
            ledDriver.set_emotion(Emotion.COMMAND_UNDERSTOOD)
            reply = execute_action(action[0])
        read_text(reply)
    else:
        ledDriver.set_emotion(Emotion.SPEECH_RECO_FAILED)
        read_text("I have not understood you, sorry")
        for i in range(3):
            ledDriver.set_emotion(Emotion.SPEECH_RECO_FAILED)
            time.sleep(0.4)
            ledDriver.set_emotion(Emotion.NONE)
            time.sleep(0.4)
    start_detector()



def interrupt_callback():
    global interrupted
    return interrupted

should_blink = False

lock = threading.Lock()

def led_blinker():
    global should_blink
    while should_blink:
        ledDriver.set_emotion(Emotion.SPEECH_RECO)
        time.sleep(0.3)
        ledDriver.set_emotion(Emotion.NONE)
        time.sleep(0.3)



def start_blinker_thread():
    global should_blink,lock
    lock.acquire()
    should_blink = True
    lock.release()
    t = threading.Thread(target=led_blinker)
    t.start()


def stop_blinker():
    global should_blink,lock
    lock.acquire()
    should_blink = False
    lock.release()

def listen_for_speech():
    # Record Audio
    r = sr.Recognizer()
    with sr.Microphone() as source:
        logger.info("Speak now")
        try:
            audio = r.listen(source, timeout=1, phrase_time_limit=10)
        except sr.WaitTimeoutError:
            logger.debug("Timeout")
            return None
    # Speech recognition using Google Speech Recognition
    try:
        start_blinker_thread()
        read_text_async("Let me think")
        text = r.recognize_google_cloud(audio, credentials_json=jsonCredentialsFile)
        stop_blinker()
        logger.debug("Speech to Text: " + text)
        return text
    except sr.UnknownValueError:
        logger.debug("Google Speech Recognition could not understand audio")
    except sr.RequestError as e:
        logger.debug("Could not request results from Google Speech Recognition service; {0}".format(e))
    stop_blinker()
    return None


# capture SIGINT signal, e.g., Ctrl+C
signal.signal(signal.SIGINT, signal_handler)

def start_detector():
    global detector
    detector = snowboydecoder.HotwordDetector(model, sensitivity=0.5)
    logger.info('Listening... Press Ctrl+C to exit')
    ledDriver.set_emotion(Emotion.IDLE)
    # main loop
    detector.start(detected_callback=hotword_detected,
                   interrupt_check=interrupt_callback,
                   sleep_time=0.03)
def stop_detector():
    global detector
    detector.terminate()

def periodic_check():
    global current_status
    now = datetime.datetime.now()
    predicted_state = usr.predict({"hour": now.hour,"minute": now.minute})
    if predicted_state != current_status["switch_1"]:
        text = ""
        if predicted_state == 0:
            execute_action("TURN_OFF_LIGHT")
            text = "I turned off the light for you, do you like my decision?"
        else:
            execute_action("TURN_ON_LIGHT")
            text = "I turned on the light for you, do you like my decision?"
        time.sleep(5)
        stop_detector()
        read_text(text)
        text = listen_for_speech()
        if "yes" not in str.lower(text): # TO improve
            correct_state = 0 if current_status["switch_1"] == 1 else 1
            usr.add_instance_to_dataset({"hour": now.hour,"minute": now.minute,
                                         "switch_status": correct_state})
            if correct_state == 0:
                execute_action("TURN_OFF_LIGHT")
            else:
                execute_action("TURN_ON_LIGHT")

def start_thread_periodic_check():
    global periodic_check_thread
    periodic_check_thread = Thread(target = periodic_check)

logger.info('Initializing hotword detection')
ledDriver.set_emotion(Emotion.IDLE)
if SHOULD_START_PERIODIC_CHECK:
    start_thread_periodic_check()
start_detector()


