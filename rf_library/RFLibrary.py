import smbus
import time

from rf_library.Message import Message
from rf_library.utils import fletcher_8

MY_ID_BYTE = 0x01
MASTER_ID_BYTE = 0x02
DEVICE_TYPE = 0x03
DEVICE_ROLE = 0x04
RADIO_ID = 8
BYTE_MESSAGE = 0xFB

class RFLibrary:

    def __init__(self):
        self.bus = smbus.SMBus(1)

    def send_message(self, message):
        if not isinstance(message,Message):
            return False
        buf = message.pack()
        for i in range (5):
            try:
                self.bus.write_i2c_block_data(RADIO_ID, buf[0], buf[1:])
                break;
            except IOError:
                print("Exception, but usually it sends the data anyway")
            
        time.sleep(1);
        try:
            bytes = self.bus.read_i2c_block_data(RADIO_ID, 0x00);
        except IOError:
            return
        for b in bytes:
            print(b)
            if b == 0x01:
                return True

    def public_initialize(self, my_id, master_id,device_type,device_role):
        return self.initialize(my_id, master_id, device_type, device_role)

    def initialize(self, my_id, master_id,device_type,device_role):

        buf = [0xFA, MY_ID_BYTE, my_id, MASTER_ID_BYTE, master_id, DEVICE_TYPE,device_type,DEVICE_ROLE, device_role, 0xD0]

        buf[9] = fletcher_8(buf, 9)
        while True:
            try:
                self.bus.write_i2c_block_data(RADIO_ID, buf[0], buf[1:])
                break
            except IOError:
                print("Exception")

        time.sleep(1);
        try:
            bytes =  self.bus.read_i2c_block_data(RADIO_ID,0x00);
        except IOError:
            return
        for b in bytes:
            print(b)
            if b == 0x01:
                self.initialized = True
                return True