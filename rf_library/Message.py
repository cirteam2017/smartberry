MESSAGE_BYTE = 0xFB
# buf[0] = MSGBYTE
# buf [1] = Receiver
# buf [end] = checksum

from rf_library.utils import fletcher_8
class Message:
    key_value = dict()
    length = 0

    def __init__(self, receiver_id, contentLength):
        self.length = contentLength + 3
        self.receiver_id = receiver_id

    def add_value(self,key,value):
        self.key_value[key] = value

    def pack(self):
        buf = []
        buf.append(MESSAGE_BYTE)
        buf.append(self.receiver_id)
        count = 2
        for key,value in self.key_value.items():
            buf.append(key)
            buf.append(value)
            count += 2
        buf.append(fletcher_8(buf,count))
        return buf