def fletcher_8(data, size):
    sum1 = 0
    sum2 = 0
    for i in range(0, size):
        sum1 += data[i];
        sum2 += sum1;
    return (sum1 & 0xF) | (sum2 << 4);