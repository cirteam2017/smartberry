from networking.NetworkManager import NetworkManager

API_KEY = "CC5bfWw4SJXiNatjdTSpOqgFdCA"
BASE_URL = "https://www.cleverbot.com/getreply?key="
class Conversation:

    conversation_id = None
    cs = None

    networkManager = NetworkManager()

    def getReply(self, input):

        jsonResponse = self.networkManager.jsonGetRequest("https://www.cleverbot.com/getreply?key="
                                                     + API_KEY
                                                     + "&input="
                                                     + input
                                                     + ("" if self.cs is None else "&cs="+self.cs))
        self.cs = jsonResponse["cs"]
        self.conversation_id = jsonResponse["conversation_id"]

        return jsonResponse["clever_output"]
