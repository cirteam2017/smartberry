import time

from rf_library.RFLibrary import RFLibrary
rf = RFLibrary()
rf.public_initialize(2,2,0,0)

time.sleep(5)

from rf_library.Message import Message
msg = Message(5,2)
msg.add_value(0x30,0)
rf.send_message(msg)