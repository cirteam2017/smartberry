from ledstrip import LEDStrip
from time import sleep
from emotion.Emotion import Emotion
CLK = 23
DAT = 22
class LedDriver:

    def __init__(self):
        self.strip = LEDStrip(CLK, DAT)

    def set_emotion(self,emotion):
        if emotion == Emotion.IDLE:
            self.set_color({'red':255,'green':255,'blue':255})
        elif emotion == Emotion.LISTENING:
            self.set_color({'red':0,'green':0,'blue':255})
        elif emotion == Emotion.COMMAND_UNDERSTOOD:
            self.set_color({'red': 0, 'green': 255, 'blue': 0})
        elif emotion == Emotion.SPEECH_RECO_FAILED:
            self.set_color({'red': 255, 'green': 0, 'blue': 0})
        elif emotion == Emotion.CONVERSATION_ANSWERING:
            self.set_color({'red': 204, 'green': 0, 'blue': 204})
        elif emotion == Emotion.SPEECH_RECO:
            self.set_color({'red': 255, 'green': 255, 'blue': 0})
        elif emotion == Emotion.NONE:
            self.strip.setcolouroff()
    def set_color(self, color):
        self.strip.setcolourrgb(color["red"], color["green"], color["blue"])



